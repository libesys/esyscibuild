##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import unittest
import subprocess
import os
import esyscibuild


print(__file__)


class TestSomething(unittest.TestCase):
    def test_something(self):
        bootstrap = esyscibuild.BootStrap()
        self.assertNotEqual(bootstrap, None)


if __name__ == "__main__":
    unittest.main()
