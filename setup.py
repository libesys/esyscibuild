##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import setuptools

# read the contents of your README file
from os import path

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setuptools.setup(
    name="ESysCIBuild",
    version="0.1.0",
    author="Michel Gillet",
    author_email="michel.gillet@libesys.org",
    description="Library to help with CI builds",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/libesys/esyscibuild",
    packages=setuptools.find_packages(where="src"),
    package_dir={"": "esyscibuild"},
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Build Tools",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent",
    ],
    keywords="ci, automation",
    python_requires=">=3",
)
