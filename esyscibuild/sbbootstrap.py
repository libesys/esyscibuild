##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import os
import sys
import subprocess

import esyscibuild.cygwin as cygwin
import esyscibuild.gitlab as gitlab
import esyscibuild.repo as repo


def print_dict(name, d):
    print("%s = {" % name)
    for entry in d.items():
        s = "    "
        if type(entry[0]) == str:
            s += '"%s"' % entry[0]
        else:
            s += "%s" % entry[0]
        s += ": "
        if type(entry[1]) == str:
            s += '"%s"' % entry[1]
        else:
            s += "%s" % entry[1]
        s += ","
        print(s)
    print("}")


class SBBootStrap:
    def __init__(self, name) -> None:
        self.m_cygwin = cygwin.Cygwin()
        self.m_gitlab = gitlab.GitLab()
        self.m_repo = repo.Repo()
        self.m_name = name
        self.m_repo_cyg_path = None
        self.m_for_ci = False
        self.m_super_build_folder = "_ci_super_build"
        self.m_super_build_path = None
        self.m_manifest = None
        self.m_project_path = None
        self.m_commit_branch = None
        self.m_debug = "ESYSCIBUILD_DEBUG" in os.environ
        self.m_print_info = False

    def set_debug(self, debug):
        self.m_debug = debug

    def get_debug(self):
        return self.m_debug

    def set_print_info(self, print_info):
        self.m_print_info = print_info

    def get_print_info(self):
        return self.m_print_info

    def get_name(self):
        return self.m_name

    def set_name(self, name):
        self.m_name = name

    def set_project_path(self, project_path):
        self.m_project_path = project_path

    def get_project_path(self):
        return self.m_project_path

    def set_commit_branch(self, commit_branch):
        self.m_commit_branch = commit_branch

    def get_commit_branch(self):
        return self.m_commit_branch

    def find_commit_branch(self):
        gitlab = self.get_gitlab()
        if gitlab is None:
            return -1
        commit_branch = gitlab.get_commit_branch()
        if commit_branch is not None:
            self.set_commit_branch(commit_branch)
            return 0
        ci_merge_ref_path = gitlab.get_ci_merge_request_ref_path()
        if ci_merge_ref_path is None:
            return -2
        ci_commit_sha = gitlab.get_ci_commit_sha()
        if ci_commit_sha is None:
            return -3
        self.set_commit_branch(ci_commit_sha)

    def set_manifest(self, manifest):
        self.m_manifest = manifest

    def get_manifest(self):
        return self.m_manifest

    def set_super_build_path(self, super_build_path):
        self.m_super_build_path = super_build_path

    def get_super_build_path(self):
        return self.m_super_build_path

    def set_super_build_folder(self, super_build_folder):
        self.m_super_build_folder = super_build_folder

    def get_super_build_folder(self):
        return self.m_super_build_folder

    def get_repo(self):
        return self.m_repo

    def get_gitlab(self):
        return self.m_gitlab

    def set_repo_cyg_path(self, repo_cyg_path):
        self.m_repo_cyg_path = repo_cyg_path
        self.m_repo.set_repo_cyg_path(repo_cyg_path)

    def get_repo_cyg_path(self):
        return self.m_repo_cyg_path

    def set_for_ci(self, for_ci=True):
        self.m_for_ci = for_ci

    def get_for_ci(self):
        return self.m_for_ci

    def is_esyssdk_dev(self):
        base_name = os.path.basename(os.getcwd())
        i = base_name.find(self.get_super_build_folder())
        if i == 0:
            return True
        return False

    def flush(self):
        sys.stdout.flush()
        sys.stderr.flush()

    def make_folder_if_needed(self):
        # This is necessary if working on ESysCIBuild or the CI scripts
        # since the code is already in a superbuild
        if "ESYS_TEMP" in os.environ:
            self.set_super_build_path(
                os.environ["ESYS_TEMP"]
                + os.sep
                + self.get_name()
                + self.get_super_build_folder()
            )
        else:
            self.set_super_build_path(os.path.abspath(self.get_super_build_folder()))
        if not os.path.exists(self.get_super_build_path()):
            os.makedirs(self.get_super_build_path())
        os.chdir(self.get_super_build_path())

    def print_info(self):
        print("Python %s" % sys.version)

    def do(self):
        if self.get_print_info():
            self.print_info()
        print("Bootstrap")
        print("cwd = %s" % os.getcwd())
        self.flush()

        if not self.m_cygwin.is_ok():
            print("CYGWIN_PATH not defined")
            self.flush()
            return -1

        if self.m_gitlab.is_ci():
            self.m_repo.set_for_ci(True)
            # Print all environment variables for debugging
            if self.get_debug():
                print_dict("env", os.environ)
            sys.stdout.flush()

        self.make_folder_if_needed()

        self.m_repo.set_repo_path(os.getcwd())
        self.m_repo.set_manifest(self.get_manifest())
        self.get_repo().log_env()
        
        result = self.m_repo.init()
        if result < 0:
            return result
        result = self.m_repo.sync()
        if result < 0:
            return result

        if os.path.exists(self.get_super_build_path()):
            os.chdir(self.get_super_build_path())

        print("cwd = %s" % os.getcwd())

        project_path = self.get_project_path()
        if project_path is None:
            project_path = "."

        commit_branch = self.get_commit_branch()
        if commit_branch is None:
            result = self.find_commit_branch()
            if result < 0:
                return 0
            commit_branch = self.get_commit_branch()

        if commit_branch is not None:
            print("Project path  = %s" % project_path)
            print("Commit branch = %s" % commit_branch)

            path = project_path

            p = os.getcwd() + os.sep + path
            p = os.path.abspath(p)
            cyg_path = self.m_cygwin.to_cyg_path(p)

            print("Git repo path = %s" % cyg_path)

            cmd = '%s\\bash.exe --login -c "cd %s && git fetch origin"' % (
                self.m_cygwin.get_bin_path(),
                cyg_path,
            )
            print(cmd)

            result = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr)
            if result.returncode != 0:
                print("ERROR %s while doing git fetch origin" %
                      result.returncode)
                return -result.returncode

            cmd = '%s\\bash.exe --login -c "cd %s && git checkout %s"' % (
                self.m_cygwin.get_bin_path(),
                cyg_path,
                commit_branch,
            )
            print(cmd)

            result = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr)
            if result.returncode != 0:
                print(
                    "ERROR %s while doing git checkout %s"
                    % (result.returncode, commit_branch)
                )
                return -result.returncode

        return 0
