##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import os
import logging
s_logger = logging.getLogger(__name__)

class SBCIBuild:
    def __init__(self, logger=s_logger):
        self.m_bootstrap = None
        self.m_build = None
        self.m_print_info = False
        self.m_logger = logger

    def info(self, s):
        if self.m_logger is None:
            return
        self.m_logger.info(s)

    def error(self, s):
        if self.m_logger is None:
            return
        self.m_logger.error(s)

    def set_bootstrap(self, bootstrap):
        self.m_bootstrap = bootstrap

    def get_bootstrap(self):
        return self.m_bootstrap

    def set_build(self, build):
        self.m_build = build

    def get_build(self):
        return self.m_build

    def set_print_info(self, print_info):
        self.m_print_info = print_info

    def get_print_info(self):
        return self.m_print_info

    def print_info(self):
        pass

    def do(self):
        self.info("do start ...")
        bootstrap = self.get_bootstrap()
        if bootstrap is None:
            self.error("boostrap is None")
            return -1
        if self.get_print_info():
            bootstrap.print_info()
            self.print_info()
        result = bootstrap.do()
        if result < 0:
            self.error("boostrap.do() return %s" % result)
            return result

        print("cwd = %s" % os.getcwd())

        build = self.get_build()
        if build is None:
            self.error("build is None")
            return -2
        build.set_base_path(bootstrap.get_super_build_path())
        result = build.do()
        if result < 0:
            self.error("build.do() return %s" % result)
            return result
        self.info("do done.")
        return 0
