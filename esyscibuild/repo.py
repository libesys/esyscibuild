##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import os
import logging
s_logger = logging.getLogger(__name__)

import sys
import subprocess
import multiprocessing
import colorama
from colorama import Fore, Back, Style

import esyscibuild.cygwin as cygwin

colorama.init()


class Repo:
    def __init__(self):
        self.m_repo_cyg_path = None
        self.m_repo_path = None
        self.m_cyg_bin_path = None
        self.m_cygwin = cygwin.Cygwin()
        self.m_for_ci = False
        self.m_repos = None
        self.m_manifest = None
        self.m_logger = s_logger

    def info(self, s):
        if self.m_logger is None:
            return
        self.m_logger.info(s)

    def error(self, s):
        if self.m_logger is None:
            return
        self.m_logger.error(s)

    def run_cmd(self, cmd_line):
        cmd = '%s\\bash.exe --login -c "%s"' % (
                self.m_cygwin.get_bin_path(),
                cmd_line
            )        
        self.info(cmd)
        result = subprocess.run(cmd,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)        
        if result.returncode != 0:
            self.error("ERROR %s while doing '%s'" %
                           result.returncode, cmd_line)
        self.info(result.stdout.decode("utf-8"))

    def log_env(self):
        print("log env ...")
        self.flush()

        self.run_cmd("which git")
        self.run_cmd("git version")
        self.run_cmd("which ssh")
        self.run_cmd("which repo")
        self.run_cmd("repo version")
        self.run_cmd("echo $PATH")
        self.run_cmd("echo $HOME")
        self.run_cmd("echo $USERNAME")
        self.run_cmd("echo $SSH_AUTH_SOCK")
        self.run_cmd("ssh -T git@gitlab.com -vvv")

        print("log env done.")
        self.flush()

    def set_manifest(self, manifest):
        self.m_manifest = manifest

    def get_manifest(self):
        return self.m_manifest

    def set_repo_cyg_path(self, repo_cyg_path):
        self.m_repo_cyg_path = repo_cyg_path

    def get_repo_cyg_path(self):
        return self.m_repo_cyg_path

    def set_repo_path(self, repo_path):
        self.m_repo_path = repo_path
        self.set_repo_cyg_path(self.m_cygwin.to_cyg_path(repo_path))

    def get_repo_path(self):
        return self.m_repo_path

    def set_for_ci(self, for_ci=True):
        self.m_for_ci = for_ci

    def get_for_ci(self):
        return self.m_for_ci

    def flush(self):
        sys.stdout.flush()
        sys.stderr.flush()

    def print_cmd(self, cmd):
        # Colorize a single line and then reset
        print(Fore.BLUE + cmd + Style.RESET_ALL)

    def print_err(self, cmd):
        # Colorize a single line and then reset
        print(Fore.RED + cmd + Style.RESET_ALL)

    def init(self):
        print("start repo init ...")
        if self.get_for_ci():
            cmd = '%s\\bash.exe --login -c "cd %s && repo --trace init --depth=1 -u %s -v"' % (
                self.m_cygwin.get_bin_path(),
                self.get_repo_cyg_path(),
                self.get_manifest(),
            )
        else:
            cmd = '%s\\bash.exe --login -c "cd %s && repo --trace init -u %s -v"' % (
                self.m_cygwin.get_bin_path(),
                self.get_repo_cyg_path(),
                self.get_manifest(),
            )
        self.flush()
        self.print_cmd(cmd)
        result = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr)
        self.flush()
        if result.returncode != 0:
            self.print_err("ERROR %s while doing repo init" %
                           result.returncode)
            return -result.returncode
        print("repo init done.")
        self.flush()
        return 0

    def sync(self):
        print("start repo sync ...")
        cpu_count = multiprocessing.cpu_count()

        if self.get_for_ci():
            cmd = (
                '%s\\bash.exe --login -c "cd %s && repo sync -f --force-sync --no-clone-bundle --no-tags -j%s"'
                % (self.m_cygwin.get_bin_path(), self.get_repo_cyg_path(), cpu_count)
            )
        else:
            cmd = '%s\\bash.exe --login -c "cd %s && repo sync -j%s"' % (
                self.m_cygwin.get_bin_path(),
                self.get_repo_cyg_path(),
                cpu_count,
            )
        self.flush()
        self.print_cmd(cmd)
        result = subprocess.run(cmd, stdout=sys.stdout, stderr=sys.stderr)
        self.flush()
        if result.returncode != 0:
            self.print_err("ERROR %s while doing repo sync" %
                           result.returncode)
            return -result.returncode
        print("repo sync done.")
        self.flush()
        return 0

    def list_repos(self, silent=False):
        if not silent:
            print("start repo list ...")
            self.flush()
        cmd = '%s\\bash.exe --login -c "cd %s && repo list"' % (
            self.m_cygwin.get_bin_path(),
            self.get_repo_cyg_path(),
        )
        if not silent:
            self.print_cmd(cmd)
            self.flush()
        result = subprocess.run(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode != 0:
            self.print_err("ERROR %s while doing repo list" %
                           result.returncode)
            return [-result.returncode, None]
        self.m_repos = {}
        result_str = result.stdout.decode("ascii")
        lines = result_str.splitlines()
        for line in lines:
            path, repo_name = line.split(":")
            path = path.strip()
            repo_name = repo_name.strip()
            self.m_repos[repo_name] = path
        if not silent:
            print("repo sync done.")
            self.flush()
        return [0, self.m_repos]

    def get_repos(self):
        return self.m_repos

    def get_path_of_repo(self, repo_name):
        if self.get_repos() is None:
            self.list_repos(True)
        return self.get_repos().get(repo_name, None)
