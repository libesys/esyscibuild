##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import os
import subprocess


class Cygwin:
    def __init__(self):
        self.m_bin_path = None
        if "CYGWIN_PATH" in os.environ:
            self.m_bin_path = os.environ["CYGWIN_PATH"] + os.sep + "bin"

    def get_bin_path(self):
        return self.m_bin_path

    def is_ok(self):
        return self.m_bin_path is not None

    def to_cyg_path(self, p):
        cmd = "%s\\cygpath %s" % (self.m_bin_path, p)
        result = subprocess.run(cmd, capture_output=True)
        if result.returncode == 0:
            return result.stdout.decode("ascii").replace("\n", "")
        return None

    def run(self, cmd):
        pass
