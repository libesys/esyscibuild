##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import importlib.util
import os
import sys

git_present = True
try:
    import git
except:
    git_present = False


class BootStrap:
    def __init__(self):
        self.m_dev_repo = None
        self.m_super_build_path = None
        self.m_super_build_cfg = None
        self.m_dev_repo_path = None
        self.m_script_file_path = None

    def set_dev_repo(self, dev_repo):
        self.m_dev_repo = dev_repo

    def get_dev_repo(self):
        return self.m_dev_repo

    def set_super_build_path(self, file_path, rel_path):
        self.m_script_file_path = file_path
        folder_path = os.path.dirname(file_path)
        abs_path = os.path.abspath(os.path.join(folder_path, rel_path))
        self.m_super_build_path = os.path.normpath(abs_path)

    def clone_dev_repo(self):
        if not git_present:
            print(": GitPython is not installed:")
            print("    pip install GitPython")
            return -1
        self.m_dev_repo_path = "_ci_dev"
        if not os.path.exists("_ci_dev"):
            repo = git.Repo.clone_from(self.get_dev_repo(), "_ci_dev")
        else:
            repo = git.Repo("_ci_dev")
            o = repo.remotes.origin
            o.fetch()
            o.pull()
        if repo is None:
            return -1
        return 0

    def call_dev_bootstrap(self):
        scripts_path = os.path.normpath(os.path.join(
            self.m_dev_repo_path, "scripts"))
        sys.path.insert(0, scripts_path)

        scripts_path = os.path.normpath(os.path.join(
            self.m_dev_repo_path, "scripts", "bootstrap.py"))

        spec = importlib.util.spec_from_file_location(
            "bootstrap", scripts_path)
        module = importlib.util.module_from_spec(spec)
        try:
            spec.loader.exec_module(module)
        except SyntaxError as ex:
            print("ERROR: couldn't load file %s" % scripts_path)
            print("    %s:" % ex)
            print("    %s" % ex.text.lstrip())
            return -1

        bootstrap = module.BootStrap()
        project_path = os.path.relpath(os.path.dirname(os.path.dirname(
            self.m_script_file_path)), self.m_dev_repo_path)
        bootstrap.set_project_path(project_path)

        result = bootstrap.do()
        if result != 0:
            print(
                "ERROR %s while calling dev bootstrap"
                % (result)
            )
            return result
        return 0

    def load_superbuild_cfg(self):
        if not os.path.exists("superbuild_cfg.py"):
            return 0
        sys.path.append(".")
        superbuild_cfg_loaded = True
        try:
            import superbuild_cfg
        except:
            superbuild_cfg_loaded = False
        if not superbuild_cfg_loaded:
            print("ERROR: failed to import superbuild_cfg.py")
            return -1
        self.m_super_build_cfg = superbuild_cfg
        try:
            sys_path_append = self.m_super_build_cfg.sys_path_append
        except:
            print("warning: superbuild_cfg.py doesn't have sys_path_append")
            return -2
        sys.path.append(sys_path_append)
        return 0

    def is_in_super_build(self):
        if "ESYSSDK_DEV_PATH" in os.environ:
            self.m_dev_repo_path = os.path.normpath(
                os.environ["ESYSSDK_DEV_PATH"])
            return True
        superbuild_path = self.m_super_build_path
        superbuild_repo_path = os.path.join(superbuild_path, ".repo")
        superbuild_esysrepo_path = os.path.join(superbuild_path, ".repo")

        if os.path.exists(superbuild_repo_path):
            self.m_dev_repo_path = superbuild_path
            return True
        if os.path.exists(superbuild_esysrepo_path):
            self.m_dev_repo_path = superbuild_path
            return True
        return False

    def do(self):
        if not self.is_in_super_build():
            result = self.clone_dev_repo()
            if result < 0:
                return result
        result = self.call_dev_bootstrap()
        if result < 0:
            return result
        result = self.load_superbuild_cfg()
        if result < 0:
            return result
        return 0
