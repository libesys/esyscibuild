##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import os


class GitLab:
    def __init__(self):
        pass

    def is_ci(self):
        if "GITLAB_CI" not in os.environ:
            return False
        if os.environ["GITLAB_CI"].lower() == "true":
            return True
        return False

    def get_project_title(self):
        if "CI_PROJECT_TITLE" not in os.environ:
            return None
        return os.environ["CI_PROJECT_TITLE"]

    def get_commit_branch(self):
        if "CI_COMMIT_BRANCH" not in os.environ:
            return None
        return os.environ["CI_COMMIT_BRANCH"]

    def get_project_name(self):
        return os.environ.get("CI_PROJECT_NAME", None)

    def get_project_path(self):
        return os.environ.get("CI_PROJECT_PATH", None)

    def get_ci_merge_request_ref_path(self):
        return os.environ.get("CI_MERGE_REQUEST_REF_PATH", None)

    def get_ci_commit_sha():
        return os.environ.get("CI_COMMIT_SHA", None)

    def is_esyssdk_dev(self):
        project_path = self.get_project_path()
        if project_path is None:
            return False
        i = project_path.find("esyssdk/dev")
        if i != -1:
            return True
        return False
