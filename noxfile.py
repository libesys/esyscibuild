##
# __legal_b__
##
# Copyright (c) 2021-2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import nox
import os
import shutil
import anybadge


def recursive_overwrite(src, dest, ignore=None):
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            os.makedirs(dest)
        files = os.listdir(src)
        if ignore is not None:
            ignored = ignore(src, files)
        else:
            ignored = set()
        for f in files:
            if f not in ignored:
                recursive_overwrite(os.path.join(src, f),
                                    os.path.join(dest, f), ignore)
    else:
        if os.path.exists(dest):
            os.remove(dest)
        shutil.copyfile(src, dest)


@nox.session(python=["3.6", "3.7", "3.8", "3.9", "3.10"])
def unit_tests(session):
    print("Python version : %s" % session.python)
    output_folder = "build/test-results-py" + session.python.replace(".", "")
    badge_file = "public/python" + \
        session.python.replace(".", "") + "-badge.svg"
    print("Output folder  : %s" % output_folder)
    session.install("unittest-xml-reporting")
    session.install("colorama")
    session.install("GitPython")
    os.makedirs("public", exist_ok=True)

    # Write badge for failed unit tests
    session.log("Write badge for failed unit tests")
    thresholds = {"Failed": "red", "Ok": "green"}
    badge = anybadge.Badge("Python " + session.python,
                           "Failed", thresholds=thresholds)
    badge.write_badge(badge_file, overwrite=True)

    result = session.run(
        "python",
        "-m",
        "xmlrunner",
        "discover",
        "-s",
        "tests",
        "-o",
        output_folder,
    )
    if result:
        # Overwrite badge after unit tests passed
        session.log("Overwrite badge after unit tests passed")
        badge = anybadge.Badge("Python " + session.python,
                               "Ok", thresholds=thresholds)
        badge.write_badge(badge_file, overwrite=True)


@nox.session()
def coverage(session):
    session.install("unittest-xml-reporting")
    session.install("colorama")
    session.install("coverage")
    session.install("GitPython")
    result = session.run(
        "coverage",
        "run",
        "-m",
        "xmlrunner",
        "discover",
        "-s",
        "tests",
        "-o",
        "build/test-results",
    )
    print("result = %s" % result)
    session.run(
        "coverage", "xml", "-o", "build/coverage.xml", "--include=esyscibuild/*.*"
    )
    session.run("coverage", "html", "-d", "build/htmlcov",
                "--include=esyscibuild/*.*")
    os.makedirs("public/htmlcov", exist_ok=True)
    recursive_overwrite("build/htmlcov/", "public/htmlcov/")
    session.run("coverage", "report", "--include=esyscibuild/*.*")


@nox.session(reuse_venv=True)
def docs(session):
    session.install("sphinx")
    session.install("sphinx-rtd-theme")
    session.install("recommonmark")
    session.install("sphinxcontrib-napoleon")
    os.chdir("docs")
    if os.path.exists("html"):
        shutil.rmtree("html")
    session.run("make", "html", external=True)
    print("makefirs")
    os.makedirs("../public", exist_ok=True)
    recursive_overwrite("build/html/", "../public/")
